﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour
{
	bool isStayingOnTrigger=false;
	public float speed;
	Move move;
	HideShowMyColor hideShow;
	bool IsEntertriggerEnded;
	GameObject effectGO;
	// Use this for initialization
	void Start ()
	{

		move = GetComponent<Move> ();

		IsEntertriggerEnded =false;
	}

	void OnEnable(){
		IsEntertriggerEnded =false;
		GlobalEventsManager.instance.OnChangeLevelState += this.StopMovement;
		GlobalEventsManager.instance.OnChangeLevelState += this.StartMovement;
	}

	void OnDisable(){
		GlobalEventsManager.instance.OnChangeLevelState  -= this.StopMovement;
		GlobalEventsManager.instance.OnChangeLevelState -= this.StartMovement;
	}


	void StopMovement(LevelState oLevelState){
		//if (Input.GetMouseButtonDown (0)) {
		if (oLevelState!=LevelState.MoveArrow && oLevelState!=LevelState.JumpToNextGroup) {
			if (move==null) {
				move = GetComponent<Move> ();
			}
//			Debug.Log (oLevelState);
			move.speed = 0;
		}
	
	}

	public void StartMovement(LevelState oLevelState){
		if (oLevelState==LevelState.MoveArrow) {
			this.GetComponent<Collider2D> ().enabled = false;
			this.transform.position = new Vector2 (LevelsManager.instance.CurrentItemsGroup.StartingPosition.x+1, LevelsManager.instance.CurrentItemsGroup.StartingPosition.y + 0.7f);

			move.speed = speed;
			this.GetComponent<Collider2D> ().enabled = true;
		} 
	
	}


	void OnTriggerStay2D(Collider2D other){
		if (!other.CompareTag ("chosen") && LevelsManager.instance.oLevelState == LevelState.LevelLost) {
			other.GetComponent<SpriteRenderer> ().color = other.GetComponent<HideShowMyColor>().highlightColor;
			other.GetComponent<Resize>().StartDecreasing ();
			if (LevelsManager.instance.CurrentLevel.EffectEnd!=EffectType.Nothing) {
				effectGO=PoolManager.instance.GetPooledObject_Effect (LevelsManager.instance.CurrentLevel.EffectEnd);
				effectGO.SetActive (true);
			}
	
		}
		if (other.CompareTag ("chosen") && LevelsManager.instance.oLevelState == LevelState.JumpToNextGroup) {
			other.GetComponent<SpriteRenderer> ().color = other.GetComponent<HideShowMyColor>().chosenColor;

			//Debug.Log ("!!!!! "+other.tag);
		}

		if (other.CompareTag ("chosen") && LevelsManager.instance.oLevelState == LevelState.CheckArrowTarget) {
			other.GetComponent<SpriteRenderer> ().color = other.GetComponent<HideShowMyColor>().chosenColor;
			if (LevelsManager.instance.CurrentLevel.EffectChoose!=EffectType.Nothing) {
				effectGO=PoolManager.instance.GetPooledObject_Effect (LevelsManager.instance.CurrentLevel.EffectChoose);
				effectGO.SetActive (true);
			}
			other.GetComponent<Resize>().StartIncreasing ();
			GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.JumpToNextGroup);
		}

		if (!other.CompareTag ("chosen") && LevelsManager.instance.oLevelState == LevelState.CheckArrowTarget) {
			GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.LevelLost);
	
		}


	}
	void OnTriggerEnter2D(Collider2D other) {

		if (!other.CompareTag("last") && IsEntertriggerEnded==false) {
			other.GetComponent<SpriteRenderer> ().color = other.GetComponent<HideShowMyColor>().highlightColor;
		}

		if (other.CompareTag("last") && IsEntertriggerEnded==false) {
			move.speed = -move.speed;
			IsEntertriggerEnded = true;
		}
	}

	void OnTriggerExit2D (Collider2D other){
		if (!other.CompareTag("last") && LevelsManager.instance.oLevelState == LevelState.MoveArrow)
			 {
			//todo
//			Debug.Log (LevelsManager.instance.oLevelState);
			other.GetComponent<SpriteRenderer> ().color = other.GetComponent<HideShowMyColor>().maskColor;
		
		}

		if (other.CompareTag ("last") && IsEntertriggerEnded == true) {

			IsEntertriggerEnded = false;
		}
			
	}

	
}

