﻿using UnityEngine;
using System.Collections;

public class PlaySoundForAWhile : MonoBehaviour
{
	public SoundType sound;
	// Use this for initialization
	void OnEnable ()
	{

		SoundManager.Instance.PlaySound(sound);
	}
	

}

