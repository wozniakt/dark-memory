﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelsManager : MonoBehaviour
{ //LevelManager is better name
	public float TimeToChangeColor;
	GameItem gameItem;
	GameObject gameItemGO;
	public Text LevelText;
	public List<Level> LevelsList;
	public Level CurrentLevel;
	public ItemsGroup CurrentItemsGroup;
	public GameObject Arrow;
	int randomItemID;
	public static LevelsManager instance;
	GameObject  GameplayContainer;
	public LevelState oLevelState;
	public GameObject GamePlaceholder;
	ParticleSystem.MinMaxGradient colorArrowParticles;
	GameObject effectGO;
	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}

	void OnDrawGizmosSelected (){

		ItemsGroup LastItemsGroup = LevelsList [LevelsList.Count - 1].ItemsGroupsList[LevelsList [LevelsList.Count - 1].ItemsGroupsList.Count - 1];
		Vector3 LastItemsGroupStartPos = LastItemsGroup.StartingPosition;
		Gizmos.DrawLine(LastItemsGroupStartPos,
			new Vector3 (

				LastItemsGroup.StartingPosition.x+LastItemsGroup.ItemsAmount*1, LastItemsGroup.StartingPosition.y, 0));
		Debug.DrawLine (LastItemsGroupStartPos,
			new Vector3 (
				
				LastItemsGroup.StartingPosition.x+LastItemsGroup.ItemsAmount*1, LastItemsGroup.StartingPosition.y, 0), Color.green);
	}

	void Start(){

		GlobalEventsManager.instance.OnChangeLevelState += this.ChangeCurrentLevelState;
	}

	// Use this for initialization
	void OnEnable ()
	{

	}

	void OnDisable(){
		GlobalEventsManager.instance.OnChangeLevelState -= this.ChangeCurrentLevelState;
	}

	public void  ChangeCurrentLevelState (LevelState levelState)
	{
		UI_Manager.instance.Rain.RainIntensity = (float)DataManager.Instance.CurrentPoints / LevelsManager.instance.LevelsList.Count;
		Arrow = GameObject.Find ("Arrow");
		oLevelState = levelState;
		switch (levelState) {
		case LevelState.WaitForColorHiding:
			break;
		case LevelState.MoveArrow:
			break;
		case LevelState.JumpToNextGroup:
			SoundManager.Instance.PlaySound(SoundType.Click);
			GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.ShowGroupItemsColor);
			if (CurrentItemsGroup != CurrentLevel.ItemsGroupsList [CurrentLevel.ItemsGroupsList.Count-1]) {
				CurrentItemsGroup = CurrentLevel.ItemsGroupsList [CurrentLevel.ItemsGroupsList.IndexOf (CurrentItemsGroup) + 1];
				GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.MoveArrow);
			} else {
				GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.LevelWon);
			}

			break;

		case LevelState.ShowGroupItemsColor:
			break;
		case LevelState.LevelWon:

			GlobalEventsManager.instance.TriggerGetPoints (DataManager.Instance.CurrentPoints+1);
			//Debug.Log ("cur points " + DataManager.Instance.CurrentPoints);

			StartCoroutine (SkipingToNExtLevel ());

			break;
		case LevelState.LevelLost:

			StartCoroutine (LoosingSequence ());
			break;

		}
	}

	IEnumerator LoosingSequence(){
		DataManager.Instance.CurrentTextProgress =Mathf.Max( DataManager.Instance.CurrentTextProgress, DataManager.Instance.CurrentPoints);
		if (DataManager.Instance.CurrentPoints%10!=0) {
			DataManager.Instance.CurrentPoints =DataManager.Instance.CurrentPoints - (DataManager.Instance.CurrentPoints % 10);
		}
		if (DataManager.Instance.CurrentPoints<0) {
			DataManager.Instance.CurrentPoints = 0;
		}
		SoundManager.Instance.PlaySound (SoundType.WrongChoice);
		GlobalEventsManager.instance.TriggerGetPoints (DataManager.Instance.CurrentPoints);
		yield return new WaitForSeconds (1);
		GameObject flashEffect = PoolManager.instance.GetPooledObject_Effect (EffectType.Flash);
		flashEffect.transform.position = Arrow.transform.position;
		flashEffect.SetActive (true);
		Arrow.transform.position=new Vector2(-300,-100);
		yield return new WaitForSeconds (2);
		Destroy (GameplayContainer);
		GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.GameLost);
	}

	IEnumerator SkipingToNExtLevel(){ //winningLevelSequence
		SoundManager.Instance.PlaySound (SoundType.GoodChoice);
		if (LevelsManager.instance.CurrentLevel.EffectLevelFinished!=EffectType.Nothing) {
			effectGO=PoolManager.instance.GetPooledObject_Effect (LevelsManager.instance.CurrentLevel.EffectLevelFinished);
			effectGO.SetActive (true);
		}
		effectGO=PoolManager.instance.GetPooledObject_Effect (EffectType.SoulDissapear);
		effectGO.transform.position=Arrow.transform.position;
		effectGO.SetActive (true);
		//yield return new WaitForSeconds (0);
		//Arrow.GetComponent<Resize>().StartDecreasing();
		Arrow.transform.position=new Vector2(-300,-200); //todo - wygasić particle - może
		yield return new WaitForSeconds (3);
		if (LevelsList.IndexOf (CurrentLevel) +1 < LevelsList.Count) {
			//CurrentLevel = LevelsList [Mathf.Max( DataManager.Instance.CurrentPoints-1,0)];	
			if (Mathf.Min (LevelsList.Count - 1, DataManager.Instance.CurrentPoints - 1)<0) {
				CurrentLevel = LevelsList [0];
			} else {
				CurrentLevel = LevelsList [Mathf.Min (LevelsList.Count - 1, DataManager.Instance.CurrentPoints )];	
					}
			Destroy (GameplayContainer);
			CreateCurrentLevel ();
		} else {
			Destroy (GameplayContainer);
			GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.Menu);
		}
	}

	#region createCurrentLevel
	public void CreateCurrentLevel(){
		//Debug.Log (Mathf.Min (LevelsList.Count - 1, DataManager.Instance.CurrentPoints - 1));
		if (Mathf.Min (LevelsList.Count - 1, DataManager.Instance.CurrentPoints - 1)<0) {
			CurrentLevel = LevelsList [0];
		} else {
			CurrentLevel = LevelsList [Mathf.Min (LevelsList.Count - 1, DataManager.Instance.CurrentPoints )];	
		}

		if (GameplayContainer!=null ){
			Destroy(GameplayContainer);
		}
		GameplayContainer =Instantiate(Resources.Load<GameObject> ("Prefabs/GamePlayContainer"));
		GameplayContainer.transform.SetParent (GamePlaceholder.transform);

		GameplayContainer.SetActive (true);
		CurrentItemsGroup = CurrentLevel.ItemsGroupsList[0];
		LevelText.gameObject.SetActive (true);
		LevelText.color = CurrentLevel.ColorText;
		LevelText.text = CurrentLevel.TextToShow;
	//	LevelText.fontSize = CurrentLevel.FontSize;
		foreach (ItemsGroup itemsGroup in CurrentLevel.ItemsGroupsList) {
			randomItemID = Random.Range (2, itemsGroup.ItemsAmount-2);
			int i = 0;

			for ( i = 0; i < itemsGroup.ItemsAmount; i++) {

				gameItemGO =Instantiate(Resources.Load<GameObject> ("Prefabs/GameItems/"+ itemsGroup.ItemsType.ToString()));
				gameItemGO.transform.position = new Vector2 (
					itemsGroup.StartingPosition.x + (gameItemGO.GetComponent<SpriteRenderer> ().bounds.size.x) * i,
					itemsGroup.StartingPosition.y);
				gameItemGO.transform.SetParent (GameplayContainer.transform);
				gameItemGO.GetComponent<SpriteRenderer> ().color = gameItemGO.GetComponent<HideShowMyColor>().baseColor;

//				Debug.Log ("!!! " + i.ToString () + (itemsGroup.ItemsAmount - 1).ToString());
				if (i==0 || i==itemsGroup.ItemsAmount-1) {
					gameItemGO.tag = "last";
					gameItemGO.GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, 0);
				}

				if (i==randomItemID) {
					gameItemGO.tag = "chosen";
					gameItemGO.GetComponent<SpriteRenderer> ().color = gameItemGO.GetComponent<HideShowMyColor>().chosenColor;
					// to debug  gameItemGO.transform.localScale = new Vector2 (2, 2);
					//Debug.Log ("chosen color: "+" "+ gameItemGO.GetComponent<HideShowMyColor>().chosenColor + gameItemGO.GetComponent<SpriteRenderer> ().color);
				}
				if (i!=0  && i!=itemsGroup.ItemsAmount-1) {
					gameItemGO.GetComponent<GameItem> ().Id = itemsGroup.Id;
				}

			}

		}
		if (CurrentLevel.EffectStart!=EffectType.Nothing) {
			effectGO=PoolManager.instance.GetPooledObject_Effect (CurrentLevel.EffectStart);
			effectGO.SetActive (true);
		
		}

		Arrow = GameObject.Find ("Arrow");
		GlobalEventsManager.instance.TriggerChangeLevelState(LevelState.WaitForColorHiding);
	}
	#endregion
}

