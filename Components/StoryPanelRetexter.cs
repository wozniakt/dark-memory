﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoryPanelRetexter : MonoBehaviour
{
	public Text StoryText;
	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable ()
	{
		SoundManager.Instance.PlaySound (SoundType.Sigh);
		foreach (Level level in LevelsManager.instance.LevelsList) {
			if (LevelsManager.instance.LevelsList.IndexOf(level)<DataManager.Instance.CurrentTextProgress) {
				StoryText.text = StoryText.text  + level.TextToShow+". ";	
			}
		}

	}

	// Update is called once per frame
	void OnDisable ()
	{
		StoryText.text = "";
	}
}

