﻿using UnityEngine;
using System.Collections;

public class HideShowMyColor : MonoBehaviour
{
	public Color baseColor;
	public Color maskColor;
	public Color chosenColor;
	public Color	 highlightColor;
	public float timeToChangeColor;
	// Use this for initialization
	void OnEnable ()
	{

		GlobalEventsManager.instance.OnChangeLevelState += this.ShowColor;
		GlobalEventsManager.instance.OnChangeLevelState += this.StartHidingColor;

	}
	void OnDisable ()
	{
		GlobalEventsManager.instance.OnChangeLevelState -= this.ShowColor;
		GlobalEventsManager.instance.OnChangeLevelState -= this.StartHidingColor;
		StopAllCoroutines ( );
	}



	public void StartHidingColor(LevelState oLevelState){
		if (oLevelState==LevelState.WaitForColorHiding) {
			StartCoroutine (HideColor (LevelsManager.instance.TimeToChangeColor));
		}

	}

	IEnumerator HideColor (float timeToChangeColor)
	{
		yield return new WaitForSeconds (timeToChangeColor);
		if (this.tag != "last" ) {
			//Debug.Log (this.tag);
			this.gameObject.GetComponent<SpriteRenderer> ().color = maskColor;
		}
		GlobalEventsManager.instance.TriggerChangeLevelState (LevelState.MoveArrow);
	}


	void ShowColor (LevelState oLevelState)
	{
		if (oLevelState==LevelState.CheckArrowTarget) {
	
			if (this.GetComponent<GameItem>().Id==LevelsManager.instance.CurrentItemsGroup.Id && this.tag!="last") {
				this.gameObject.GetComponent<SpriteRenderer> ().color = baseColor;
			}
			if (this.GetComponent<GameItem>().Id==LevelsManager.instance.CurrentItemsGroup.Id && this.tag=="chosen") {
				this.gameObject.GetComponent<SpriteRenderer> ().color = chosenColor;
			}

		}

	}


}

