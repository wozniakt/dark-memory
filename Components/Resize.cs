﻿using UnityEngine;
using System.Collections;

public class Resize : MonoBehaviour
{
	public float interval;
	public float decreaseStepValue;
	public float increaseStepValue;
	// Use this for initialization
	void OnEnable ()
	{


	}
	void OnDisable ()
	{

		StopAllCoroutines ();
	}

	public void StartDecreasing(){
		StartCoroutine (Decreasing (decreaseStepValue));
	
	}

	public void StartIncreasing(){
		StartCoroutine (Increasing (increaseStepValue));

	}
	// Update is called once per frame
	IEnumerator Decreasing(float decreaseStepValue)
	{
		do {
			yield return new WaitForSeconds (interval);
			this.transform.localScale=new Vector2(this.transform.localScale.x+decreaseStepValue,this.transform.localScale.y+decreaseStepValue);
		} while (this.transform.localScale.x>decreaseStepValue);

		this.gameObject.SetActive (false);
	}

	IEnumerator Increasing(float increaseStepValue)
	{
		do {
			
			yield return new WaitForSeconds (interval);
			this.transform.localScale=new Vector2(this.transform.localScale.x+increaseStepValue,this.transform.localScale.y+increaseStepValue);
		} while (this.transform.localScale.x<1.5f);


	}
}

