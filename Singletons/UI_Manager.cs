﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DigitalRuby.RainMaker;
using UnityEngine.Events;
using System.Collections.Generic;


public class UI_Manager : MonoBehaviour
{
	public List<SoundType> GameOverSounds;
	public AndroidNativeFunctions androidNativeFunc;
	GlobalEventsManager globalEventsManager;
	public Button Button_ResetProgress;
	public Button Button_Menu_Start, Button_Menu_Quit;
	public Button  Button_Pause,Button_Pause_Restart, Button_Pause_BackToMenu, Button_Pause_Resume;
	public Button  Button_GameOver_Restart, Button_GameOver_Quit, Button_GameOver_GoToMenu;

	public GameObject Panel_Hud, Panel_Gameplay;
	public GameObject Panel_GameOver, Panel_Pause, Panel_Menu;
	public RainScript2D Rain;
	public static string PREFAB_PATH = "Prefabs/";
	public Text currentPointsText;
//	public Text currentCheckpointText;
//	public Text nextCheckpointText;

	public static UI_Manager instance;
	public GameState oGameState;
	public GameObject StoryPanel;

	public GameObject GamePlaceholder;
	GameObject gamePlayPanel;

	public GameObject ContentLevelsUIScroll;
	public List<string> TextsAndroid=new List<string>();
	Animator Img_RandomEvent_Animator,PanelBackground_Animator;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
		//androidNativeFunc = GetComponent<AndroidNativeFunctions> ();
	}

	void Start(){
		AddListenersToButtons ();
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints += this.UpdateHud;
		globalEventsManager.TriggerOnChangeGameState (GameState.Menu);
		globalEventsManager.TriggerGetPoints (0);
	}
	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints -= this.UpdateHud;
	}

	public void  ChangeGameStateUI (GameState gameState)
	{ 
		//Debug.Log ((float)DataManager.Instance.CurrentPoints / LevelsManager.instance.LevelsList.Count);
		Rain.RainIntensity = (float)DataManager.Instance.CurrentPoints / LevelsManager.instance.LevelsList.Count;
		//Debug.Log ((float)DataManager.Instance.CurrentPoints / LevelsManager.instance.LevelsList.Count);
		Panel_Menu.SetActive (false); 
		//Panel_Gameplay.SetActive (false); 
		Panel_Pause.SetActive (false);  
		Panel_GameOver.SetActive (false); 
		Panel_Hud.SetActive (false);
		oGameState = gameState;
//		Debug.Log (oGameState);
		switch (oGameState) {
		case GameState.Menu:
			LevelsManager.instance.StopAllCoroutines ();
			ShowAndroidToast(TextsAndroid[Random.Range(Mathf.Max(0,DataManager.Instance.CurrentPoints-10),Mathf.Max(10, DataManager.Instance.CurrentPoints))]);
			Panel_Menu.SetActive (true); 
			GameplayContainerDestroyOrInstantitate (false);
			//UpdateUnlockedLevelsUI ();
			break;
		case GameState.GameOn:
			GamePlaceholder.SetActive (true);
			Panel_Menu.SetActive (false); 
			GameplayContainerDestroyOrInstantitate (true);
			Panel_Hud.SetActive (true);
			//LevelsManager.instance.CurrentLevel.SubLevelCounter=0;
			LevelsManager.instance.CreateCurrentLevel ();
			break;
		case GameState.GameQuit:
			break;
		case GameState.GamePaused:
			Panel_Pause.SetActive (true);
			Panel_Hud.SetActive (false);
			GamePlaceholder.SetActive (false);
			break;

		case GameState.GameResumed_AfterPause:
			
			Panel_Pause.SetActive (false); 
			Panel_Hud.SetActive (true);
			GamePlaceholder.SetActive (true);
			break;

		case GameState.GameRestart_AfterPause:
			LevelsManager.instance.StopAllCoroutines ();
			GameplayContainerDestroyOrInstantitate (false);
			GamePlaceholder.SetActive (true);
			GameplayContainerDestroyOrInstantitate (true);
			Panel_GameOver.SetActive (false);
			Panel_Hud.SetActive (true);
		
			//LevelsManager.instance.CurrentLevel.SubLevelCounter=0;
			LevelsManager.instance.CreateCurrentLevel ();//todo probably
			break;

		case GameState.GameLost:
			LevelsManager.instance.StopAllCoroutines ();
			Panel_GameOver.SetActive (true);
			int rndSound = Random.Range (0, GameOverSounds.Count);
			//Debug.Log (rndSound);
			SoundManager.Instance.PlaySound (GameOverSounds[rndSound]);
			//GameplayContainerDestroyOrInstantitate (false);
			if (gamePlayPanel!=null ){
				Destroy(gamePlayPanel);
			}
			Panel_Hud.SetActive (false);
			//LevelsManager.instance.CurrentLevel.SubLevelCounter=0;
			break;

		case GameState.GameRestart_AfterLost:
			LevelsManager.instance.StopAllCoroutines ();
			GamePlaceholder.SetActive (true);
			Panel_GameOver.SetActive (false);
			Panel_Hud.SetActive (true);
			//GameplayContainerDestroyOrInstantitate (false);
			//GameplayContainerDestroyOrInstantitate (true);
			//LevelsManager.instance.CurrentLevel.SubLevelCounter=0;
			LevelsManager.instance.CreateCurrentLevel ();//todo probably
			break;
		}
	}

	void AddListenersToButtons(){
		Button_ResetProgress.onClick.RemoveAllListeners ();
		Button_ResetProgress.onClick.AddListener (() => ResetGameProgress());

		currentPointsText.GetComponent<Button> ().onClick.RemoveAllListeners ();
		currentPointsText.GetComponent<Button> ().onClick.AddListener (() => ShowStory (true));

		StoryPanel.GetComponent<Button> ().onClick.RemoveAllListeners ();
		StoryPanel.GetComponent<Button> ().onClick.AddListener (() => ShowStory (false));

		Button_Menu_Start.onClick.RemoveAllListeners ();
		Button_Menu_Start.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameOn));
		Button_Menu_Quit.onClick.RemoveAllListeners ();
		Button_Menu_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));

		Button_Pause.onClick.RemoveAllListeners ();
		Button_Pause.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GamePaused));
		Button_Pause_BackToMenu.onClick.RemoveAllListeners ();
		Button_Pause_BackToMenu.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_Pause_BackToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_Pause_Resume.onClick.RemoveAllListeners ();
		Button_Pause_Resume.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameResumed_AfterPause));
		Button_Pause_Resume.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_Pause_Restart.onClick.RemoveAllListeners ();
		Button_Pause_Restart.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_Pause_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterPause));

		Button_GameOver_GoToMenu.onClick.RemoveAllListeners ();
		Button_GameOver_GoToMenu.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_GameOver_GoToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_GameOver_Quit.onClick.RemoveAllListeners ();
		Button_GameOver_Quit.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_GameOver_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));
		Button_GameOver_Restart.onClick.RemoveAllListeners ();
		Button_GameOver_Restart.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
		Button_GameOver_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterLost));

		Button[] ButArray;
		ButArray = Transform.FindObjectsOfType (typeof(Button)) as Button[];
		foreach (Button btn in ButArray) {
			if (btn.name!="ButtonStopArrow") {
				btn.onClick.AddListener (() => SoundManager.Instance.PlaySound(SoundType.Click));
			}
			//btn.onClick.RemoveAllListeners ();

		}
	}

	public void ShowStory(bool setToActive){
		if (DataManager.Instance.oGameState==GameState.Menu) {
			StoryPanel.SetActive (setToActive);
		}
	}
	public void ShowAndroidToast(string message)
	{
		AndroidNativeFunctions.ShowToast(message);
	}


	public void ResetGameProgress(){
		AndroidNativeFunctions.ShowAlert ("Erase all memories?", "Are you sure?", "Yes, I want start again", "No! It was mistake", "", ShowAlertAction);
		//AndroidNativePlugin.SendLocalNotification ("loremIpsum1loremIpsum2loremIpsum3loremIpsum", "HELLO THERE, 123456789, abcdefghijklmnopqrstuwxyz");
	}
	void ShowAlertAction(DialogInterface w){
		
		if (w==DialogInterface.Positive) {
			DataManager.Instance.CurrentPoints = 0;
			GlobalEventsManager.instance.TriggerGetPoints (0);
			AndroidNativeFunctions.ShowToast("I have lost all memories...");
		}
	}

	public void UpdateHud(int currentPoints){
		currentPointsText.text ="Recovered memories  "+currentPoints+"/"+(LevelsManager.instance.LevelsList.Count-1).ToString();
//		currentCheckpointText.text ="checkpoint "+DataManager.Instance.CurrentCheckpoint.ToString ();
//		nextCheckpointText.text ="next "+DataManager.Instance.NextCheckpoint.ToString ();

	}

	public void GenerateLevels_UI(){
		foreach (Level oLevel in LevelsManager.instance.LevelsList) {
			GameObject levelPrefab =Instantiate(Resources.Load<GameObject> ("Prefabs/UI/LevelPrefab"));
			levelPrefab.transform.SetParent (ContentLevelsUIScroll.transform);
			levelPrefab.transform.localScale = new Vector3 (1, 1, 1);
		}
	}

	public void UpdateUnlockedLevelsUI(){

		foreach (Level level in LevelsManager.instance.LevelsList) {
			foreach (GameObject levelUI in GameObject.FindGameObjectsWithTag("LevelUI") ){
				if (levelUI.name==LevelsManager.instance.LevelsList.IndexOf(level).ToString()) {
					levelUI.GetComponent<Image> ().sprite = Resources.Load ("Sprites/Levels/" + level.ItemsGroupsList [0].ItemsType.ToString (), typeof(Sprite)) as Sprite;
					int levelNo = int.Parse(levelUI.name);
					//Debug.Log (levelNo + "DataManager.Instance.Highscore " + DataManager.Instance.CurrentCheckpoint);
//					if (levelNo<=DataManager.Instance.CurrentCheckpoint) {
//						level.isUnlocked = true;
//					}
//					if (levelNo>DataManager.Instance.CurrentCheckpoint) {
//						level.isUnlocked = false;
//					}


//					if (level.isUnlocked == false) {
						levelUI.GetComponent<Image> ().color = Color.black;
//					}
//					if (level.isUnlocked == true) {
						levelUI.GetComponent<Image> ().color = Color.white;
//					}

				}
			}

		}
	}

	void GameplayContainerDestroyOrInstantitate(bool shouldBeActive){
		if (shouldBeActive) {
			//gamePlayPanel =Instantiate(Resources.Load<GameObject> ("Prefabs/GamePlayContainer"));
			//gamePlayPanel.transform.SetParent (GamePlaceholder.transform);
		}
		if (!shouldBeActive) {
		//	Destroy(gamePlayPanel,1);
		}


	}
}
