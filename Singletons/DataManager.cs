﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DataManager : MonoBehaviour
{

	GlobalEventsManager globalEventsManager;
	public static DataManager Instance;
	public GameState oGameState;

	public int CurrentTextProgress;
	public int CurrentPoints;
//	public int CurrentCheckpoint;
//	public int NextCheckpoint;
	// Use this for initialization
	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy (gameObject);
		}
		CurrentPoints = PlayerPrefs.GetInt ("CurrentPoints",0);
		CurrentTextProgress = PlayerPrefs.GetInt ("CurrentTextProgress",0);
	}

	void Start(){

//		CurrentPoints = CurrentCheckpoint;
//		NextCheckpoint = CurrentCheckpoint + 3;
		UI_Manager.instance.UpdateHud (CurrentPoints);
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameState;
		globalEventsManager.OnGetPoints += this.AddPoints;

	}
	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ChangeGameState;
		globalEventsManager.OnGetPoints -= this.AddPoints;
	}

	public void  ChangeGameState (GameState gameState)
	{
		oGameState = gameState;
		Time.timeScale = 1;
		switch (oGameState) {
		case GameState.Menu:
			//CurrentPoints = CurrentCheckpoint;
			break;
		case GameState.GameOn:
			//CurrentPoints = CurrentCheckpoint;
			//LevelsManager.instance.CreateCurrentLevel ();
			break;
		case GameState.GameQuit:
			break;
		case GameState.GamePaused:
			Time.timeScale = 0;
			break;
		case GameState.GameResumed_AfterPause:
			Time.timeScale = 1;
			break;
		case GameState.GameLost:
			//CurrentPoints = CurrentCheckpoint;
			Time.timeScale = 1;
			break;
		case GameState.GameRestart_AfterLost:
			//CurrentPoints = CurrentCheckpoint;
			Time.timeScale = 1;
			break;
		case GameState.GameRestart_AfterPause:
			//CurrentPoints = CurrentCheckpoint;
			Time.timeScale = 1;
			break;
		}
	}

	void AddPoints(int newPoints){
//		Debug.Log (CurrentTextProgress);
		CurrentTextProgress = Mathf.Max(CurrentTextProgress, CurrentPoints);
		//Highscore=Mathf.Max(Highscore, newPoints);
		CurrentPoints =newPoints;
//		if (CurrentPoints>=CurrentCheckpoint) {
//			CurrentCheckpoint = NextCheckpoint;
//			NextCheckpoint = CurrentCheckpoint + 3;
//		}

//		if (newPoints%10==0) {
//			DataManager.Instance.Highscore = newPoints;
//			//UI_Manager.instance.UpdateHud (Highscore);
//		} else {
//			//DataManager.Instance.CurrentPoints = DataManager.Instance.Highscore;
//			//UI_Manager.instance.UpdateHud (CurrentPoints);
//		}
		CurrentPoints=Mathf.Min(CurrentPoints,LevelsManager.instance.LevelsList.Count-1);
		PlayerPrefs.SetInt("CurrentTextProgress",CurrentTextProgress);
		PlayerPrefs.SetInt("CurrentPoints",CurrentPoints);
		UI_Manager.instance.UpdateHud (CurrentPoints);
	//	Debug.Log (CurrentTextProgress);
	}

}
