﻿using UnityEngine;
using System.Collections;


public enum GameState {Menu, GameOn, GamePaused,GameRestart_AfterPause,GameLost, GameRestart_AfterLost, GameResumed_AfterPause, GameQuit}
public enum SoundType{Click, Jump, Hit, Whisper, Sigh, Scream, Laugh,ScarySound, WrongChoice,GoodChoice, ExplosionSoft,
	GameOver1,GameOver2,GameOver3,GameOver4,GameOver5,GameOver6,GameOver7}

public enum LevelState {WaitForColorHiding, MoveArrow, CheckArrowTarget , JumpToNextGroup,ShowGroupItemsColor,LevelWon, LevelLost}
public enum EffectType {Nothing,SoulDissapear, Flash, Blood1, Blood2, ManLove, SadMan}