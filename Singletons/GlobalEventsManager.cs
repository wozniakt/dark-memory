﻿using UnityEngine;
using System.Collections;

public class GlobalEventsManager : MonoBehaviour
{

	public static GlobalEventsManager instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}


	public delegate void UpdateData();
	public event UpdateData eUpdateData;

	public void TriggerUpdateData(){
		if (eUpdateData!=null) {
			eUpdateData (); 
		}
	}

	public delegate void ChangeGameState(GameState gameState);
	public event ChangeGameState OnChangeGameState;

	public void TriggerOnChangeGameState (GameState gameState)
	{
		if (OnChangeGameState != null) {
			OnChangeGameState (gameState); 
		}
	}

	public delegate void ChangeLevelState(LevelState levelState);
	public event ChangeLevelState OnChangeLevelState;

	public void TriggerChangeLevelState (LevelState levelState)
	{
		if (OnChangeLevelState != null) {
			OnChangeLevelState (levelState); 
		}
	}

	public delegate void GetPoints(int newPoints);
	public event GetPoints OnGetPoints;

	public void TriggerGetPoints (int newPoints)
	{
		if (OnGetPoints != null) {
			OnGetPoints (newPoints); 
		}
	}

	//public delegate void StopArrow();
	//public event StopArrow OnStopArrow;
	public void TriggerStopArrow ()
	{
	//	Debug.Log ("Button pressed");
		TriggerChangeLevelState (LevelState.CheckArrowTarget);
//		Debug.Log (LevelsManager.instance.oLevelState);
	}

}
