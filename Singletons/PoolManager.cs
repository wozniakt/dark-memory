﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject  EffectsPool;
	public bool WillGrow = true;


	List<GameObject> pooledEffects;

	void Awake()
	{  
		instance = this;
		pooledEffects = new List<GameObject> ();

	}
	void Start(){
		createEffects(5,EffectType.Flash);
		//createEffects(5,EffectType.Skull);


		//createCubes (4, "Enemy1");
	}

	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			//Debug.Log (effectType.ToString() +"<-eefectType/item " + item.name);
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}



	//

}