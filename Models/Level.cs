﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


[System.Serializable]
public class Level 
{


	[SerializeField]
	List<ItemsGroup> _itemsGroupsList;
	public List<ItemsGroup> ItemsGroupsList {
		get		{
			return _itemsGroupsList; 
		}
		set {
			_itemsGroupsList = value;
		}
	}

		[SerializeField]
		private Color _colorText;
		public Color ColorText{
			get		{
			return _colorText; 
			}
			set {
			_colorText = value;
			}
		}




//	[SerializeField]
//	private int _fontSize;
//	public int FontSize{
//		get		{
//			return _fontSize; 
//		}
//		set {
//			_fontSize = value;
//		}
//	}

	[SerializeField]
	private string _textToShow;
	public string TextToShow{
		get		{
			return _textToShow; 
		}
		set {
			_textToShow = value;
		}
	}

	[SerializeField]
	private EffectType _effectStart;
	public EffectType EffectStart{
		get		{
			return _effectStart; 
		}
		set {
			_effectStart = value;
		}
	}


	[SerializeField]
	private EffectType _effectEnd;
	public EffectType EffectEnd{
		get		{
			return _effectEnd; 
		}
		set {
			_effectEnd = value;
		}
	}

	[SerializeField]
	private EffectType _effectChoose;
	public EffectType EffectChoose{
		get		{
			return _effectChoose; 
		}
		set {
			_effectChoose = value;
		}
	}

	[SerializeField]
	private EffectType _effectLevelFinished;
	public EffectType EffectLevelFinished{
		get		{
			return _effectLevelFinished; 
		}
		set {
			_effectLevelFinished = value;
		}
	}

//	[SerializeField]
//	bool _isUnlocked;
//	public bool isUnlocked {
//		get		{
//			return _isUnlocked; 
//		}
//		set {
//			_isUnlocked = value;
//		}
//	}
//
//	[SerializeField]
//	int _subLevelCounter;
//	public int SubLevelCounter {
//		get		{
//			return _subLevelCounter; 
//		}
//		set {
//			_subLevelCounter = value;
//		}
//	}
//
//	[SerializeField]
//	int _subLevelCounterMax;
//	public int SubLevelCounterMax {
//		get		{
//			return _subLevelCounterMax; 
//		}
//		set {
//			_subLevelCounterMax = value;
//		}
//	}

}






