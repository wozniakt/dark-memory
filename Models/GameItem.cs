﻿using UnityEngine;
using System.Collections;
[System.Serializable]

public class GameItem:MonoBehaviour  
{

	public enum ItemType {Star, Planet, Heart};

	[SerializeField]
	ItemType _type;
	public ItemType Type {
		get		{
			return _type; 
		}
		set {
			_type = value;
		}
	}

	[SerializeField]
	int _rewardAmount;
	public int RewardAmount {
		get		{
			return _rewardAmount; 
		}
		set {
			_rewardAmount = value;
		}
	}

	[SerializeField]
	int _id;
	public int Id {
		get		{
			return _id; 
		}
		set {
			_id = value;
		}
	}
}

