﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class ItemsGroup 
{
	public enum ItemType {Star, Planet, Heart};

	[SerializeField]
	string _itemsType;
	public string ItemsType {
		get		{
			return _itemsType; 
		}
		set {
			_itemsType = value;
		}
	}

	[SerializeField]
	int _itemsAmount;
	public int ItemsAmount {
		get {
			return _itemsAmount; 
		}
		set {
			_itemsAmount = value;
		}
	}


	[SerializeField]
	int _id;
	public int Id {
		get {
			return _id; 
		}
		set {
			_id = value;
		}
	}

	[SerializeField]
	Vector2 _startingPosition;
	public Vector2 StartingPosition {
		get {
			return _startingPosition; 
		}
		set {
			_startingPosition = value;
		}
	}
}

